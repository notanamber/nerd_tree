#!/usr/bin/env python

from src.cmdline_parser import cmd_args
from src.tree_find import NerdTreeFind

if __name__ == '__main__':
  # print(cmd_args.path)
  # print(cmd_args.show_children_nodes)
  # print(cmd_args.show_father_subtree_info)
  path = cmd_args.path
  pattern = cmd_args.pattern
  show_children_nodes = cmd_args.show_children_nodes
  show_subtree_info = cmd_args.show_subtree_info
  use_re = cmd_args.regular_expression

  # opts for generatign the tree
  opts = {
      'items_to_exclude' : cmd_args.exclude or [],
      'follow_symbolic_link': True,
      }

  if show_subtree_info:
    ## force show_children_nodes -> True
    show_children_nodes = True

  # opts for find
  find_opts = {
      'dont_show_children_nodes': not show_children_nodes,
      'show_subtree_info': show_subtree_info,
      'type' : cmd_args.type,
      'items_to_include' : cmd_args.include or [],
      'use_regular_expression' : use_re,
      }

  ntree = NerdTreeFind(path, opts=opts)
  ntree.find(pattern, opts=find_opts)
