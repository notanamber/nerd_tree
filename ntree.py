#!/usr/bin/env python

from src.ntree_cmdline_parser import cmd_args
from src.tree import NerdTree

if __name__ == '__main__':
  # print(cmd_args.path)
  # print(cmd_args.show_children_nodes)
  # print(cmd_args.exclude)
  path = cmd_args.path
  list_dir_first = cmd_args.list_directories_first
  colorize = cmd_args.colorize
  follow_symbolic_link = cmd_args.follow_symbolic_link

  # opts for generatign the tree
  opts = {
      'items_to_exclude' : cmd_args.exclude or [],
      'list_dir_first' : list_dir_first,
      'colorize' : colorize,
      'follow_symbolic_link': follow_symbolic_link,
      }

  ntree = NerdTree(path, opts=opts)
  ntree.print()
