import argparse
from pathlib import Path

cmd_parser = argparse.ArgumentParser(
    prog='vfind',
    description='A visual version of unix find command in which the results are shown in a tree',
    epilog="Thanks for using it! :)",
    )

cmd_parser.add_argument("path")
cmd_parser.add_argument("pattern")
cmd_parser.add_argument("-t", "--type")
# if -s is found in the command line cmd_args.show_children_nodes is True
cmd_parser.add_argument("-s", "--show-children-nodes", action="store_true")
cmd_parser.add_argument("-S", "--show-subtree-info", action="store_true")
## more than one occurrencies of -e option are appended in a list
cmd_parser.add_argument("-e", "--exclude", action="append")

# supporting regular expression otherwise use shell expansions
cmd_parser.add_argument("-re", "--regular-expression", action="store_true")

cmd_parser.add_argument("-i", "--include", action="append")

# start the command line parsing
cmd_args = cmd_parser.parse_args()

p = cmd_args.path
path = Path(p)

if not path.exists():
  print(f'Path: {p} dont\' exists')
  raise SystemExit(1)

