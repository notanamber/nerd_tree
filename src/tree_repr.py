import os

from src.colors import RED, YELLOW, GREEN, CYAN , BLUE, PURPLE

EMPTY_TREE_LINE_LENGTH = 160
EMPTY_TREE_LINE = ' ' * EMPTY_TREE_LINE_LENGTH

VERTICAL_CONNECTOR = '│'
### same length
CHILD_CONNECTOR = '├─ '
LAST_CHILD_CONNECTOR = '└─ '
### is the length of child connector and last_child_connector
LEVEL_INDENT = 3
# ITEMS_TO_EXCLUDE = ['__pycache__']
ITEMS_TO_EXCLUDE = []

KB = 1024
MB = 1024*1024
GB = 1024*1024*1024

def mount_str_at(source_str, index, mount_str):
  l = len(mount_str)
  return source_str[:index] + mount_str + source_str[index+l:]

def level2indent(l):
  if not l:
    return 0

  return LEVEL_INDENT * l

def produce_treeline(prec_seps, lsep):

  tree_line = EMPTY_TREE_LINE[:]
  lseps = prec_seps[:]
  lseps.append(lsep)

  for level_n, (sep, item) in enumerate(lseps):
    tomount = sep
    if item['name']:
      if item.get('color_formatter'):
        item_name = item['color_formatter'](item['name'])
      else:
        item_name = item['name']
      tomount = sep + item_name

      if item.get('is_symlink') and item.get('target', ''):
        if item.get('target_color_formatter'):
          tomount += ' -> %s' % ((item['target_color_formatter'](item['target']),))
        else:
          tomount += ' -> %s' % ((item['target'],))

      # if item.get('type') == 'd':
      #   if item.get('nchildren') is not None:
      #     tomount += '(%s)' % (item['nchildren'],)

    tree_line = mount_str_at(tree_line, level2indent(level_n), tomount)
  
  return tree_line.rstrip()

def format(num, unit=None):
  if num/GB >=1 or unit == 'G':
    unit = 'G'
    human_number = round(num/GB, 2)
  elif num/MB >=1 or unit == 'M':
    unit = 'M'
    human_number = round(num/MB, 2)
  elif num/KB >=1 or unit == 'K':
    unit = 'K'
    human_number = round(num/KB, 2)
  else:
    unit = 'B'
    human_number = num
  # unit = 'b'
  # human_number = num
  
  return human_number, unit


def get_color_formatter(t):
  '''
    @param        t      string can be 'd', 'f', 'l', 'x'
  '''
  if t == 'd':
    return BLUE
  if t == 'x':
    return GREEN
  if t == 'l':
    return CYAN
  return None
  
