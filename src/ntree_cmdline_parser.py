import argparse
from pathlib import Path

cmd_parser = argparse.ArgumentParser(
    prog='ntree',
    description='A python version (probabily worst) of tree unix command',
    epilog="Thanks for using it! :)",
    )

cmd_parser.add_argument("path")
# cmd_parser.add_argument("pattern")
cmd_parser.add_argument("-e", "--exclude", action="append")
cmd_parser.add_argument("-l", "--list-directories-first", action="store_true")

cmd_parser.add_argument("-L", "--follow-symbolic-link", action="store_true")

# colorize
cmd_parser.add_argument("-c", "--colorize", action="store_true")

# start the command line parsing
cmd_args = cmd_parser.parse_args()

p = cmd_args.path
path = Path(p)

if not path.exists():
  print(f'Path: {p} dont\' exists')
  raise SystemExit(1)
