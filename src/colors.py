def RED(s):
  return "\033[01;31m%s\033[00m" % (str(s))

def YELLOW(s):
  return "\033[01;33m%s\033[00m" % (str(s))

def GREEN(s):
  return "\033[01;32m%s\033[00m" % (str(s))

def CYAN(s):
  return "\033[01;36m%s\033[00m" % (str(s))

def BLUE(s):
  return "\033[01;34m%s\033[00m" % (str(s))

def PURPLE(s):
  return "\033[01;35m%s\033[00m" % (str(s))

