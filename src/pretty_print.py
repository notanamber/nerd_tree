import json

def pp(json_obj, indent=2):
  dumps = json.dumps(json_obj, indent=indent)
  print(dumps)
