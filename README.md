# Visual Find

**vfind** is a tool written in python based on *nerd_tree*.<br />
Its purpose (and base idea) is to blend the unix command *find* with the *tree* command.<br />
You can search in a particular subtree for files or folders that match a regular expression and other criteria, the results will be shown graphically in a tree (in a similar way that the tree command does).<br />

## Installation on your system

In my opinion the better solution is:<br />
1. Clone the repository in your home folder
2. Go to your home folder and make a symbolic link to the command in your ~/bin folder
```
  cd ~/nerd_tree
  ln -s ~/nerd_tree/vfind.py ~/bin/vfind
```

## How to use

**vfind** has 2 mandatory arguments, the *path* of subtree from which to begin the search and the *search* pattern.
The **search** pattern can be:

- an expression whith wild card supporting the shell expansion, the default
- a regular expression, using the flag -re

```
vfind ~/nerd_tree/ -re '(.*)rem(.*)'
```

all items matching the pattern are show in the result tree: in this specific case any name containing **rem** substring<br />
The same results above can be achieved using the command version with the shell expansion:

```
vfind ~/nerd_tree/ '*rem*'
```
![vfind ~/nerd_tree/ '*rem*'](/screenshot/vfind_1.jpg)

You can use as pattern any valid regular expression, for example pattern as this

```
vfind ~/nerd_tree/ -re '(.*)rem|tree(.*)'
```

The folder matching the pattern are show collapsed with information about the total number of files and the size of the subtree.
If you want to show the subtree expanded you can use the flag **-s**

```
vfind ~/nerd_tree/ -re '(.*)rem(.*)' -s
```

![vfind ~/nerd_tree/ -re '(.*)rem(.*)' -s](/screenshot/vfind_2.jpg)

## Include/Exclude path

You can include a set of results selecting specific branch (or paths) with the option **-i** (include).
In this case the match is made exactly on the pattern provided, for example:

```
vfind ~/nerd_tree/ -re '(.*)py' -i src
```

include the results that along the path have the name **src**.<br />
**NOTE** the command above is equivalent to 

```
vfind ~/nerd_tree/ '*py' -i src             
```

You can include the -i more than once as in this case:

```
vfind ~/nerd_tree/ -re '([0-9][0-9])|(^tree_(.*))' -i src -i .git
```

the above command will search for files or folders with a **two digits** name or with the name beginning with **tree_**
only in the path ***src*** or ***.git***


In a similar manner you can exclude from the results tree paths or items with the option **-e**.

> The **-e** option can be any valid pattern that support the shell expansion

too in this case you can specify more than once the option -e 

```
vfind ~/nerd_tree/ -re '(.*)py' -e src
```

```
vfind -e .git -e __pycache__  ~/nerd_tree/ 'ntree*'
```

![vfind -e .git -e __pycache__  ~/nerd_tree/ 'ntree*'](/screenshot/vfind_3.jpg)
